package image_evolver;

import java.awt.Color;

import processing.core.PApplet;

public class Button {
	private int posX;
	private int posY;
	private int width;
	private int height;
	private ButtonState state;
	private String textOn;
	private String textOff;
	private PApplet a;
	private Color color;
	
	public Button(int x, int y, int width, int height, String textOn, String textOff, PApplet applet) {
		this.posX = x;
		this.posY = y;
		this.width = width;
		this.height = height;
		this.textOff = textOff;
		this.textOn = textOn;
		this.state = ButtonState.OFF;
		this.a = applet;
		this.color = new Color(100, 100, 255);
		this.draw();
	}
	
	public Button(int x, int y, int width, int height, String textOn, String textOff, PApplet applet, Color color) {
		this.posX = x;
		this.posY = y;
		this.width = width;
		this.height = height;
		this.textOff = textOff;
		this.textOn = textOn;
		this.state = ButtonState.OFF;
		this.a = applet;
		this.color = color;
		this.draw();
	}
	
	public void draw() {
		a.fill(this.color.getRed(), this.color.getGreen(), this.color.getBlue());
		a.noStroke();
		a.rect(this.posX, this.posY, this.width, this.height);
		a.fill(255);
		a.textSize(16);
		if (this.state == ButtonState.ON) {
			a.text(this.textOn, this.posX + 5, this.posY + 15);
		} else {
			a.text(this.textOff, this.posX + 5, this.posY + 15);
		}
	}
	
	public boolean clicked(int mouseX, int mouseY) {
		if (mouseX >= this.posX  && mouseX < this.posX + this.width && mouseY >= this.posY && mouseY < this.posY + this.height) {
			this.state = (this.state == ButtonState.ON) ? ButtonState.OFF :	ButtonState.ON;
			this.draw();
			return true;
		}
		return false;
	}
	
	public ButtonState getState() {
		return this.state;
	}
	
	public void setColor(Color color) {
		this.color = color;
	}
	
	public enum ButtonState {
		ON,
		OFF
	}
}	