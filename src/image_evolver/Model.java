package image_evolver;

import image_evolver.Button.ButtonState;

import java.util.ArrayList;
import java.util.List;

import processing.core.PApplet;

public class Model extends PApplet {

	private static final long serialVersionUID = 1L;
	private List<Population> populations;
	private int populationSize;
	private boolean isRunning;
	private int generationCount;
	private int evolutionCount;
	private int maxEvolution;
	private float mutationRate;
	private List<byte[]> targets;
	private Button stopButton;
	private Button showButton;
	private Button showOrderButton;
	private boolean showPopulation;
	private int n;
	private Grid grid;
	private Image targetImage;
	private InformationExhibitor exhibitor;
	
	public void setup() {
		this.size(displayWidth, displayHeight); // fullscreen
		this.populationSize = 100;
		this.isRunning = true;
		this.generationCount = 0;
		this.evolutionCount = 0;
		this.maxEvolution = 50;
		this.mutationRate = (float) 0.05;
		this.n = 5;
		this.populations = new ArrayList<Population>();
		this.showPopulation = true;
		this.exhibitor = new InformationExhibitor(this, this.width / 2 + 10, 10, this.width  / 2 - 80, this.height - 100);
		
		// Extracts the targets images from the file
		ImageReader imageReader = new ImageReader("lines.pbm");
		Mat mat = imageReader.readImage();
		targets = mat.getSquares(this.n);
		
		// Create grid of CA, it binds a cell on the grid with the corresponding CA
		double maxW = this.width / 2;
		double maxH = this.height - 150;		
		grid = new Grid(10, 10, maxW, maxH, this.populations, targets, mat, this);
		
		// Fill the population
		for (Population pop: this.populations) {
			for (int i=0; i<this.populationSize; i++) {
				pop.add(new CellularAutomata(this.n));
			}
		}

		this.stopButton = new Button(50, this.height - 130, 150, 30, "Continue", "Stop", this);
		this.showButton = new Button(250, this.height - 130, 150, 30, "Show evolution", "Show image", this);
		this.showOrderButton = new Button(450, this.height - 130, 150, 30, "Show Order", "Show Order", this);
		this.targetImage = new Image(10, 10, maxW, maxH, this, mat);
	}
	
	public void draw() {
		this.clear();
		this.background(255);
		if (this.isRunning) {
			// Generate next population
			this.populations
				.parallelStream()
				.forEach(p -> p.evolve());
//			for (Population p: this.populations) {
//				p.evolve();
//			}
//			Logger.print(this, "Populations evolved");

			this.evolutionCount ++;
			if (this.evolutionCount == this.maxEvolution) {
//				// Crossover
				this.populations
					.parallelStream()
					.forEach(p -> p.crossover());
//				for (Population p: this.populations) {
//					p.crossover();
//					Logger.print(this, "crossover");
//				}
				Logger.print(this, "crossover");
//
//				// Mutation
				this.populations
					.parallelStream()
					.forEach(p -> p.mutate(this.mutationRate));
//				for (Population p: this.populations) {
//					p.mutate(this.mutationRate);
//				}
				Logger.print(this, "mutation");
				this.evolutionCount = 0;
				this.generationCount++;
				this.grid.notifyNewGeneration(generationCount);
////				try {
////					Thread.sleep(1000);
////				} catch (InterruptedException e) {
////					// TODO Auto-generated catch block
////					e.printStackTrace();
//				}
				Logger.print(this, "Starting new Generation");
			}
		}
		this.stopButton.draw();
		this.showButton.draw();
		this.showOrderButton.draw();
		if (this.grid.tick()) {
			this.exhibitor.notifySolutionFound();
			this.isRunning = false;
		}
		if (this.showPopulation) {
			// Show current population
			this.grid.show();
		} else {
			// Show target image
			this.targetImage.show();
		}
		this.exhibitor.show(this.evolutionCount, this.generationCount);
	}
	
	public void stop() {
		super.stop();
	}
	 
	public void mousePressed() {
		if (this.stopButton.clicked(mouseX, mouseY)) {
			if (this.stopButton.getState() == ButtonState.ON) {
				this.isRunning = false;
			} else {
				this.isRunning = true;
			}
		}
		if (this.showButton.clicked(mouseX, mouseY)) {
			if (this.showButton.getState() == ButtonState.ON) {
				this.showPopulation = false;
			} else {
				this.showPopulation = true;
			}
		}
		if (this.showOrderButton.clicked(mouseX, mouseY)) {
			if (this.showOrderButton.getState() == ButtonState.ON) {
				this.grid.showOrder(true);
			} else {
				this.grid.showOrder(false);
			}
		}
		Cell cellClicked = this.grid.clicked(mouseX, mouseY);
		if (cellClicked != null) {
			this.exhibitor.addCellFocus(cellClicked);
		}
	}
}
