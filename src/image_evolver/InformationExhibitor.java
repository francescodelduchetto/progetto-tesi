package image_evolver;

import processing.core.PApplet;

public class InformationExhibitor {
	
	private PApplet p;
	private int posX;
	private int posY;
	private int width;
	private int heigth;
	private boolean solutionFound;
	
	private Cell cellFocused;
	
	public InformationExhibitor(PApplet applet, int posX, int posY, int width, int heigth) {
		this.p = applet;
		this.posX = posX;
		this.posY = posY;
		this.width = width;
		this.heigth = heigth;
		this.solutionFound = false;
		
	}
	
	public void show(int evolution, int generation) {
		p.pushMatrix();
		p.translate(posX, posY);
		p.fill(80);
		p.rect(0, 0, width, heigth);
		// Here the things to show

		p.fill(180, 255, 0);
		p.noStroke();
		p.textSize(18);
		p.text("Evolution: " + evolution, 20, 30);
		p.text("Generation: " + generation, 200, 30);
		
		if (this.solutionFound) {
			p.fill(255, 0, 0);
			p.noStroke();
			p.textSize(20);
			p.text("Evolution finished", 20, 130);
		}
		// end
		p.popMatrix();
	}
	
	public void addCellFocus(Cell cell) {
		this.cellFocused = cell;
	}
	
	public void notifySolutionFound() {
		this.solutionFound = true;
	}
}
