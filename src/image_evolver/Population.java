package image_evolver;

import image_evolver.Grid.Neighboor.Direction;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Population {
	
	private List<CellularAutomata> automatas;
	
	private CellularAutomata successfulCA;
	
	private CellularAutomata bestCA;
	private int nSameBestCA;
			
	private int nSurvivors;
	
	private byte[] target;
	
	private int order;
	
	// 0: north, 1: east, 2: south, 3: west
	private byte[][] borders;
	
	Population(List<CellularAutomata> population, byte[] target) {
		this.order = (int)Math.sqrt(target.length);
		this.automatas = population;
		this.successfulCA = null;
		this.nSurvivors = 10;
		this.bestCA = new CellularAutomata(order);
		this.target = target;
		this.borders = new byte[4][order * order];
		this.nSameBestCA = 0;
	}
	
	public void evolve() {
		if (this.keepLiving()) {
			this.automatas
				.parallelStream()
				.forEach(ca -> ca.evolve(this.target, this.borders));
//
//			for (CellularAutomata ca : this.population) {
//				ca.evolve(this.target);
//				ca.evaluateFitness(this.target);
//			}
		}
	}
	
	public void crossover() {
		if (this.keepLiving()) {

			// Sort the population according to their fitness
			Collections.sort(this.automatas, new Comparator<CellularAutomata>() {
				@Override
				public int compare(CellularAutomata o1, CellularAutomata o2) {
					return o2.getBestFitness() - o1.getBestFitness();
				}
			});	
			
			// Search for successful automatas
			for (int i=0; i<this.automatas.size(); i++) {
				CellularAutomata ca = this.automatas.get(i);
				// Select the successful CA
				if (ca.isSuccessful()) {
					this.addSuccessful(ca);
				}
			}

			// Save the fittest automatas
			List<CellularAutomata> tempPopulation = new ArrayList<CellularAutomata>(this.automatas.subList(0, this.nSurvivors));
			for (int i=0;i<tempPopulation.size(); i++) {
				tempPopulation.set(i, tempPopulation.get(i).clone());
			}
			
			/*
			 * If the best automata remains the same for 15 generations then we replace
			 * the automatas (randomizing the rules) that have fitness below the mean 
			 * fitness of the population
			 */
			if (this.bestCA.getRulesNumber() == tempPopulation.get(0).getRulesNumber()) {
				if (this.nSameBestCA++ == 15) {
					double meanFitness = this.automatas.stream()
							.mapToInt(CellularAutomata::getBestFitness)
							.average()
							.getAsDouble();
					this.automatas.stream()
						.filter(ca -> ca.getBestFitness() < meanFitness)
						.forEach(ca -> ca.randomizeRules());
					this.nSameBestCA = 0;
					Logger.print(this, "Population replaced");
				}
			} else {
				this.nSameBestCA = 0;
			}
			this.bestCA = tempPopulation.get(0);
			
			// Mating and generation of children
			while (tempPopulation.size() < this.automatas.size()) {
				CellularAutomata partner1 = this.automatas.get(this.getRandomCAIndex());
				CellularAutomata partner2 = this.automatas.get(this.getRandomCAIndex());
				tempPopulation.addAll((Collection<? extends CellularAutomata>) partner1.crossover(partner2));
			}
			
			// Remove references
			for (int i=0; i<this.automatas.size(); i++) {
				this.automatas.set(i, null);
			}
			this.automatas = null;
			
			// Re-initialize the survivors
			for (int i=0; i<this.nSurvivors; i++) {
				tempPopulation.get(i).initialize();
			}
			
			this.automatas = tempPopulation;
			
//			Logger.print(this, "Population size: " + this.population.size());
		}
	}
	
	/*
	 * Returns a random index of the population.
	 * The greater is the fitness of an automata, the greater is the 
	 * chance of selecting its index.
	 */
	private int getRandomCAIndex() {
		int tot = this.automatas
					.stream()
					.mapToInt(CellularAutomata::getBestFitness)
					.sum();
		int rand = (int)Math.floor(Math.random() * tot);
		for (int i=0; i<this.automatas.size(); i++) {
			rand -= this.automatas.get(i).getBestFitness();
			if (rand < 0) {
				return i;
			}
		}
		return 0;
	}
	
	private void addSuccessful(CellularAutomata ca) {
//		for (CellularAutomata sca : this.successfulCA) {
//			if (sca.getRulesNumber() == ca.getRulesNumber()) {
//				return;
//			}
//		}
		this.successfulCA = ca;
	}
	
//	public void addSuccessful(List<CellularAutomata2D> list) {
//		for (CellularAutomata2D ca : list) { 
//			this.addSuccessful(ca);
//		}
//	}
	
	private boolean keepLiving() {
		if (this.successfulCA != null) {
			return false;
		}
		return true;
	}
	
//	public List<CellularAutomata> getAllSuccessful() {
//		return this.successfulCA;
//	}
	
	public CellularAutomata getSuccessful() {
//		if (this.successfulCA.size() > 0) {
//			this.successfulCA.get(0);
//		}
		return this.successfulCA;
	}
	
	public CellularAutomata getBestCA() {
		return this.bestCA;
	}
	
	public byte[] getTarget() {
		return this.target.clone();
	}
	
	public void mutate(float rate) {
		if (this.keepLiving()) {
			for (int i=this.nSurvivors; i<this.automatas.size(); i++) {
				this.automatas.get(i).mutate(rate);
			}
		}
	}
	
	public CellularAutomata get(int index) {
		return this.automatas.get(index);
	}
	
	public void add(CellularAutomata automata) {
		this.automatas.add(automata);
	}
	
	public void setBorder(byte[] b, Direction d) {
		switch (d) {
		case NORTH:
			this.borders[0] = b;
			break;
		case EAST:
			this.borders[1] = b;
			break;
		case SOUTH:
			this.borders[2] = b;
			break;
		case WEST:
			this.borders[3] = b;
			break;
		}
		Logger.print(this, "Border updated");
	}
}
