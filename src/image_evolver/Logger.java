package image_evolver;

public class Logger {

	public static void print(Object cls, String message) {
		System.out.println(cls.getClass().getCanonicalName() + " =>  " + message);
	}
}
