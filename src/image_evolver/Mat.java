package image_evolver;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Mat {

	byte[][] mat;
	int width;
	int heigth;
	int nQuadColumns;
	int nQuadRows;
	
	public Mat(int width, int heigth) {
		this.mat = new byte[heigth][width];
		this.width = width;
		this.heigth = heigth;
	}
	
	public void setRow(int i, byte[] row) {
		if (i >= 0 && i < this.heigth) {
			this.mat[i] = row.clone();
		}
	}
	
	public byte[][] getMat() {
		return this.mat;
	}
	
	public void reshape (int width, int heigth) {
		byte[][] nMat = new byte[heigth][width];
		for (int i=0; i<heigth; i++) {
			if (this.heigth > i) {
				nMat[i] = Arrays.copyOf(this.mat[i], width);
			} else {
				nMat[i] = new byte[width];
				Arrays.fill(nMat[i], (byte)0);
//				nMat[i] = row;
				Logger.print(this, "empty row added");
			}
		}
		this.width = width;
		this.heigth = heigth;
		this.mat = nMat;
		Logger.print(this, "Mat reshaped: " + heigth + "x" + width);
//		Logger.print(this, toString());
	}
	
	public List<byte[]> getSquares(int order) {
		if (this.width%order != 0 || this.heigth%order != 0) {
			this.reshape((int)Math.ceil((double)this.width / order) * order, (int)Math.ceil((double)this.heigth / order) * order);
		}
		List<byte[]> squares = new ArrayList<byte[]>();
		for (int i=0; i<this.heigth; i+=order) {
			for (int j=0; j<this.width; j+=order) {
				byte m[] = new byte[order * order];
				for (int k=0; k<m.length; k++) {
					int x = j + (k % order);
					int y = i + (int) Math.floor(k / order);
					m[k] = this.mat[y][x];
				}
				squares.add(m);
			}
		}
		this.nQuadColumns = this.width / order;
		this.nQuadRows = this.heigth / order;
		Logger.print(this, "Created " + squares.size() + " squares");
		return squares;
	}
	
	public byte[] toArray() {
		byte array[] = new byte[this.width * this.heigth];
		for (int i=0; i<this.heigth; i++) {
			for (int j=0; j<this.width; j++) {
				array[i*this.width+j] = this.mat[i][j];
			}
		}
		return array;
	}
	
	@Override
	public String toString() {
		String str = new String();
		str = "size: " + this.heigth + "x" + this.width + '\n';
		for (int i=0; i<this.heigth; i++) {
			str += i + ": " + Arrays.toString(this.mat[i]);
			str += '\n';
		}
		return str;
	}
}
