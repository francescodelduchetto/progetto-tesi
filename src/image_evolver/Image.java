package image_evolver;

import processing.core.PApplet;

public class Image {
	private double x;
	private double y;
	private PApplet p;
	private double width;
	private Mat mat;
	private byte[] scheme;
	
	public Image(double posX, double posY, double width, double heigth, PApplet applet, Mat mat) {
		this.x = posX;
		this.y = posY;
		this.p = applet;
		this.mat = mat;
		
		if (mat.width > mat.heigth) {
			this.width = width;
		} else {
			this.width = heigth / mat.heigth * mat.width;
		}
	}
	
	public void show() {
		double pointWidth = this.width / this.mat.width;
		p.pushMatrix();
		p.translate((float)this.x, (float)this.y);
		scheme = this.mat.toArray();
//		System.out.println(scheme.length);
		for (int i=0; i<scheme.length; i++) {
			if (scheme[i] == 1) {
				p.fill(0);
			} else {
				p.fill(255);
			}
//			p.stroke(255, 0, 0);
			p.rect((float)(i % this.mat.width * pointWidth), (float)(Math.floor(i / this.mat.width) * pointWidth), (float)pointWidth, (float)pointWidth);
//			System.out.println("x: " + (int)(i % order * pointWidth + getActualX()) + ", y: " + Math.floor(i / order) * pointWidth + getActualY());
		}
		p.popMatrix();
	}
	
	public boolean clicked(int mouseX, int mouseY) {
		if (mouseX >= this.x && mouseX < this.x + this.width && mouseY >= this.y && mouseY < this.y + this.width) {
			return true;
		}
		return false;
	}
}
