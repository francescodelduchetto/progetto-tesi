package image_evolver;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class ImageReader {

	private File file;
	private FileInputStream inputStream;

	public ImageReader(String name) {
		this.file = new File(name); 
		
		Logger.print(this, file.getAbsolutePath());
		
		this.openInputStream();
	}
	
	private boolean openInputStream() {
		try {
			this.inputStream = new FileInputStream(file);
			Logger.print(this, "Open input stream");
			return true;
		} catch (FileNotFoundException e) {
			Logger.print(this, "File not found, cannot open input stream");
		}
		return false;
	}
	
	public Mat readImage() {
		byte mat[] = new byte[100];
		int width = 0, heigth = 0;
		try {
			this.inputStream.read(mat, 0, 2);
		
			// First line : type of file
			if (mat[0] == "P".getBytes()[0] && mat[1] == "1".getBytes()[0]) {
				Logger.print(this, "Plain file format");
			} else if (mat[0] == "P".getBytes()[0] && mat[1] == "4".getBytes()[0]) {
				Logger.print(this, "Raw file format");
			} else {
				Logger.print(this, "Unknown file format");
				return null;
			}
			this.inputStream.read();
		
			// Second line : comments
			this.inputStream.read(mat, 0, 1);
			if (mat[0] == "#".getBytes()[0]) {
				while (this.inputStream.read() != '\n') {}
			}
			
			// Third line : image size	
			int i;
			for (i=0; ; i++) {
				this.inputStream.read(mat, i, 1);
				if (mat[i] == ' ') {
					break;
				}
			}
			width = Integer.parseInt(new String(Arrays.copyOfRange(mat, 0, i), StandardCharsets.UTF_8));
			Logger.print(this, "Width: " + width);
			for (i=0; ; i++) {
				this.inputStream.read(mat, i, 1);
				if (mat[i] == '\n') {
					break;
				}
			}
			heigth = Integer.parseInt(new String(Arrays.copyOfRange(mat, 0, i), StandardCharsets.UTF_8));
			Logger.print(this, "Heigth: " + heigth);
		
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Mat res = new Mat(width, heigth);
		byte row[] = new byte[width];
		try {
			int count = 0;
			while (count < width * heigth) {
				byte val = (byte) this.inputStream.read();
				if (val != '\n') {
					row[count % width] = (byte) ((val == '1') ? 1 : 0);
					count ++;
					if (count % width == 0) {
						res.setRow((int)Math.floor(count / width)-1, row);
					}
//					Logger.print(this, count + Arrays.toString(row));
				}
				
			}
			Logger.print(this, "Image read");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return res;
	}
}
