package image_evolver;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public class CellularAutomata implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private byte[] scheme;
	
	private int n;
	
	private int[] ruleSet;
	
	private int bestFitness;
	
	private byte[] bestScheme;
	
	private int evolutionStep;
	
	private int bestEvolutionStep;
	
	public CellularAutomata(int n) {
		// Scheme initialization
		this.scheme = new byte[n*n];
		this.n = n;
		
		// Rules initialization
		this.ruleSet = new int[32];		// Von Neumann neighborhood. 2^5 rules.
		this.randomizeRules();
		
		this.initialize();
		this.bestScheme = this.scheme.clone();
		this.bestFitness = 0;
		this.bestEvolutionStep = 0;
	}
	
	/*
	 * Initialize the scheme with all zeros and a one in the center
	 */
	public void initialize() {
		Arrays.fill(this.scheme, (byte)0);
		this.scheme[this.scheme.length / 2] = 1;

		this.evolutionStep = 0;
//		this.bestEvolutionStep = this.evolutionStep;
	}
	
	public void randomizeRules() {
		for (int i=0; i<this.ruleSet.length; i++) {
			this.ruleSet[i] = (int) Math.round(Math.random());
		}
	}
	
	// Generate the next step of ca
	public int evolve(byte[] target, byte[][] borders) {
		byte[] newScheme = new byte[this.n * this.n];
		for (int i=0; i<this.scheme.length; i++) {
			newScheme[i] = this.evolvePoint(i, borders);
		}
		this.scheme = newScheme;
		this.evolutionStep++;
		this.evaluateFitness(target);
		return this.evolutionStep;
	}
	
	/*
	 * Evolve the specific point using the borders provided
	 * for the outer points.
	 */
	private byte evolvePoint(int index, byte[][] borders) {
		int result;
		String configuration;
	    int u, d, l, r, c = this.scheme[index];
	    
	    if (index < this.n) u = borders[0][index + this.n * (this.n - 1)]; 
	    else u = this.scheme[index-this.n];
	    if (index%this.n == this.n-1) r = borders[1][index - this.n + 1];
	    else r = this.scheme[index+1];
	    if (index >= this.n*(this.n-1)) d = borders[2][index - this.n * (this.n - 1)];
	    else d = this.scheme[index+this.n];
	    if (index%this.n == 0) l = borders[3][index + (this.n-1)];
	    else l = this.scheme[index-1];
	    
	    configuration = "" + u + l + c + r + d;
	    result = Integer.parseInt(configuration, 2);
		
		return (byte)this.ruleSet[result];
	}
	
	public void mutate(double rate) {
		for (int i=0; i<this.ruleSet.length; i++) {
			if (Math.random() < rate) {
				this.ruleSet[i] = (this.ruleSet[i] == 0) ? 1 : 0;
			}
		}
	}
	
	public int evaluateFitness(byte[] target) {
		int score = 0;
		for (int i=0; i<this.scheme.length; i++) {
			score += (target[i] == this.scheme[i]) ? 1 : 0;
		}
		if (score > this.bestFitness) {
			this.bestFitness = score;
			this.bestScheme = this.scheme;
			if (this.bestEvolutionStep < this.evolutionStep) {
				this.bestEvolutionStep = this.evolutionStep;
			}
		}
		return score;
	}
	
	// Return two children
	public Collection<CellularAutomata> crossover(CellularAutomata p) {
		CellularAutomata partner = (CellularAutomata) p;
		Collection<CellularAutomata> prole = new ArrayList<>();
		CellularAutomata firstChild = new CellularAutomata(this.n);
		CellularAutomata secondChild = new CellularAutomata(this.n);
		
		for (int i=0; i<this.ruleSet.length; i++) {
			// con midpoint
//			int midPoint = (int) Math.random() * this.scheme.length;
//			if (i < midPoint) {
//				((CellularAutomata) firstChild).getRuleSet()[i] = this.ruleSet[i];
//				((CellularAutomata) secondChild).getRuleSet()[i] = partner.getRuleSet()[i];
//			} else {
//				((CellularAutomata) secondChild).getRuleSet()[i] = this.ruleSet[i];
//				((CellularAutomata) firstChild).getRuleSet()[i] = partner.getRuleSet()[i];
//			}
			// senza
			if (Math.random() < 0.5) {
				((CellularAutomata) firstChild).getRuleSet()[i] = this.ruleSet[i];
				((CellularAutomata) secondChild).getRuleSet()[i] = partner.getRuleSet()[i];
			} else {
				((CellularAutomata) firstChild).getRuleSet()[i] = partner.getRuleSet()[i];
				((CellularAutomata) secondChild).getRuleSet()[i] = this.ruleSet[i];
			}
		}
		CellularAutomata thirdChild = firstChild.clone();
		CellularAutomata fourthChild = secondChild.clone();
		prole.add(firstChild);
		prole.add(secondChild);
		prole.add(thirdChild);
		prole.add(fourthChild);
		for (CellularAutomata ca : prole) {
			ca.initialize();
		}
		return prole;
	}
	
	
	public int[] getRuleSet() {
		return this.ruleSet.clone();
	}
	
	
	public int getRulesNumber() {
		int m = 0;
		for (int i=0; i<this.ruleSet.length; i++) {
			m += (1 << i) * this.ruleSet[i];
		}
		return m;
	}
	
	
	public byte[] getScheme() {
		return this.scheme.clone();
	}
	
	
	public int getOrder() {
		return this.n;
	}
	
	
	public int getBestFitness() {
		return this.bestFitness;
	}
	
	
	public byte[] getBestScheme() {
		return this.bestScheme.clone();
	}
	
	
	public int getBestEvolutionStep() {
		return this.bestEvolutionStep;
	}
	
	public CellularAutomata clone() {
		CellularAutomata cloneCA = new CellularAutomata(this.n);
		cloneCA.scheme = this.scheme.clone();
		cloneCA.ruleSet = this.ruleSet.clone();
		cloneCA.bestEvolutionStep = this.bestEvolutionStep;
		cloneCA.bestFitness = this.bestFitness;
		cloneCA.bestScheme = this.bestScheme.clone();
		return cloneCA;
	}
	
	public boolean isSuccessful() {
		return (this.bestFitness == this.n * this.n);
	}
}
