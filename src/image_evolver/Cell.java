package image_evolver;

import image_evolver.Grid.Neighboor;

import java.util.List;

import processing.core.PApplet;

public class Cell {
	private double x;
	private double y;
	private double width;
	private PApplet p;
	private Population population;
	private Grid grid;
	private int order;
	private byte[] scheme;
//	private boolean focused;
	private boolean showOrder;
	boolean isSuccessful;
	private int currentGeneration;
	
	private int label;
	
	public Cell(double posX, double posY, double width, PApplet applet, Population population, Grid grid) {
		this.x = posX;
		this.y = posY;
		this.width = width;
		this.p = applet;
		this.population = population;
		this.grid = grid;
//		this.focused = false;
		this.label = -1;
		this.isSuccessful = false;
		this.currentGeneration = 1;
	}
	
	public boolean tick() {
		// Tick
		if (this.population.getSuccessful() != null) {
			isSuccessful = true;
			this.scheme = this.population.getSuccessful().getBestScheme().clone();	
		} else {
			this.scheme = this.population.getBestCA().getBestScheme().clone();
		}
		this.order = this.population.getBestCA().getOrder();
		return this.isSuccessful;
	}
	
	public void show() {
		//Show
		int border = 0;
		double pointWidth = (this.width - border) / order;
		p.pushMatrix();
		p.translate((float)this.x, (float)this.y); 
//		  // border
//		if (this.focused) {
//			p.stroke(255, 0, 0);
//			p.rect(0, 0, (float)(pointWidth * this.width), (float)(pointWidth * this.width));
//		}
//		System.out.println(scheme.length);
		for (int i=0; i<scheme.length; i++) {
			if (scheme[i] == 1) {
				if (isSuccessful) 
					p.fill(0);
				else 
					p.fill(0, 0, 150) ;
			} else {
				if (isSuccessful) 
					p.fill(255);
				else 
					p.fill(255, 255, 128) ;
			}

			if (this.showOrder) {
				p.noStroke();
				p.fill((255 / this.currentGeneration) * this.label, this.label == -1? 0 : 0, this.label == -1? 0 : 255);
			}
			
			p.noStroke();
			p.rect((float)(i % order * pointWidth), (float)(Math.floor(i / order) * pointWidth), (float)pointWidth, (float)pointWidth);
		}
		p.popMatrix();
	}
	
	public boolean clicked(int mouseX, int mouseY) {
		if (mouseX >= this.x && mouseX < this.x + this.width && mouseY >= this.y && mouseY < this.y + this.width) {
//			this.focused = true;
			return true;
		}
//		this.focused = false;
		return false;
	}
	
	public int getLabel() {
		return this.label;
	}
	
	public Population getPopulation() {
		return this.population;
	}
	
	public byte[] getScheme() {
		return this.scheme.clone();
	}
	
	/* 
	 * When a neighbor found its optimal solution we can use its
	 * borders to evolve our scheme.
	 */
	public void notifyNeighboorSuccess(Neighboor n) {
		this.population.setBorder(n.getCell().getScheme(), n.getDirection());
	}
	
	/*
	 * When the optimal solution is found we notify the neighbors that
	 * we have finished evolving.
	 */
	public void notifyNewGeneration(int generation) {
		if (this.isSuccessful && this.label == -1) {
			List<Neighboor> neigs = this.grid.getNeighboors(this);
			this.label = generation;
			neigs.stream().forEach(n -> n.notifySuccess(this));
		}
		this.currentGeneration = generation;
	}
	
	public void showOrder(boolean really) {
		this.showOrder = really;
	}
	
//	public void setScheme(byte[] scheme, boolean isSuccessful) {
//		this.scheme = scheme;
//		this.isSuccessful = isSuccessful;
//	}
}
