package image_evolver;

import image_evolver.Grid.Neighboor.Direction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import processing.core.PApplet;

public class Grid {
	
	private double pWidth;
	private double pHeigth;
	private int posX;
	private int posY;
	private int columns;
	private int rows;
	private List<Population> populations;
	private List<byte[]> targets;
//	private Mat mat;
//	private PApplet applet;
	private Cell cells[];

	public Grid(int posX, int posY, double width, double heigth, List<Population> pops, List<byte[]> targets, Mat mat, PApplet applet) {
		this.pWidth = width;
		this.pHeigth = heigth;
		this.posX = posX;
		this.posY = posY;		
		this.columns = mat.nQuadColumns;
		this.rows = mat.nQuadRows;
		this.populations = pops;
		this.targets = targets;
//		this.mat = mat;
//		this.applet = applet;
		
		// Create grid
		double cellSize;
		if (mat.width > mat.heigth) {
			cellSize = this.pWidth / columns;
		} else {
			cellSize = this.pHeigth / rows;
		}
		this.cells = new Cell[this.targets.size()];
		boolean found;
		for (int i=0; i<targets.size(); i++) {
			found = false;
			for (Population pop: this.populations) {
				if (Arrays.equals(pop.getTarget(), targets.get(i))) {
					this.cells[i] = new Cell(posX + i % columns * cellSize, posY + (int) Math.floor(i / columns) * cellSize, 
							cellSize, applet, pop, this);
					found = true;
					break;
				}
			}
			if (!found) {
				List<CellularAutomata> auts = new ArrayList<CellularAutomata>();
				Population pop = new Population(auts, targets.get(i));
				this.populations.add(pop);
				this.cells[i] = new Cell(posX + i % columns * cellSize, posY + (int) Math.floor(i / columns) * cellSize, 
						cellSize, applet, pop, this);
			}
		}
		Logger.print(this, "Populations created, size: " + this.populations.size());
	}
	
	public boolean tick() {
		boolean finished = true;
		for (int i=0; i<this.cells.length; i++) {
			finished &= cells[i].tick();
		}
		return finished;
	}
	
	public void show() {
		for (int i=0; i<this.cells.length; i++) {
			this.cells[i].show();
		}
	}
	
	public List<Neighboor> getNeighboors(Cell cell) {
		int index = 0;
		for (int i=0; i<this.cells.length; i++) {
			if (this.cells[i].equals(cell)) {
				index = i;
				break;
			}
		}
		List<Neighboor> neigs = new ArrayList<Grid.Neighboor>();		
		if (index > this.columns) {
			neigs.add(new Neighboor(Direction.NORTH, this.cells[index - this.columns]));
		}
		if (index % this.columns < this.columns - 1) {
			neigs.add(new Neighboor(Direction.EAST, this.cells[index + 1]));
		}
		if (index < this.columns * (this.rows - 1)) {
			neigs.add(new Neighboor(Direction.SOUTH, this.cells[index + this.columns]));
		}
		if (index % this.columns > 0) {
			neigs.add(new Neighboor(Direction.WEST, this.cells[index - 1]));
		}
		return neigs;
	}
	
	public Cell clicked(int mouseX, int mouseY) {
		if (mouseX >= this.posX && mouseX < this.posX + this.pWidth && mouseY >= this.posY && mouseY < this.posY + this.pHeigth) {
			for (int i=0; i<this.cells.length; i++) {
				if (this.cells[i].clicked(mouseX, mouseY)) {
					return this.cells[i];
				}
			}
		}
		return null;
		
	}
	
	public void showOrder(boolean really) {
		for (int i=0; i<this.cells.length; i++) {
			this.cells[i].showOrder(really);
		}
	}
	
	public void notifyNewGeneration(int generation) {
		for (int i=0; i<this.cells.length; i++) {
			this.cells[i].notifyNewGeneration(generation);
		}
	}

	
	public static class Neighboor {
		public enum  Direction{
			NORTH, EAST, SOUTH, WEST 
		};
		
		private Direction direction;
		private Cell cell;
		
		public Neighboor(Direction dir) {
			this.direction = dir;
		}

		public Neighboor(Direction dir, Cell cell) {
			this.direction = dir;
			this.cell = cell;
		}
		
		public void notifySuccess(Cell c) {
			Neighboor opposite = new Neighboor(this.getOppositeDirection(this.direction), c);
			this.cell.notifyNeighboorSuccess(opposite);
		}
		
		public Direction getOppositeDirection(Direction d) {
			switch (d) {
			case NORTH:
				return Direction.SOUTH;
			case EAST:
				return Direction.WEST;
			case SOUTH:
				return Direction.NORTH;
			case WEST:
				return Direction.EAST;
			default:
				return null;
			}
		}

		public Direction getDirection() {
			return direction;
		}

		public void setDirection(Direction direction) {
			this.direction = direction;
		}

		public Cell getCell() {
			return cell;
		}

		public void setCell(Cell cell) {
			this.cell = cell;
		}		
	}
	
}
